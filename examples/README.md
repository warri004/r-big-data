R-Parallel
==========

Welcome to some example R codes showing the capability of R in a parallel environment.
The following examples are available:

* [R-parallel](./r-parallel.html)
* [OpenCL](./openCL.html)
* [Open Grid Engine (cluster)](./cluster.html)
* [3D plotting](./3d.html)
