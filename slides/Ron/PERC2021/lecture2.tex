\documentclass[fleqn]{beamer}
\mode<presentation>
%% \mode<handout>

\usepackage{amsmath, mathtools}
\usepackage{booktabs}
\usepackage{wasysym}
\usepackage{verbatim}
\usepackage[nogin]{Sweave}          % for R input and output
\usepackage{comment, latexsym}
\usepackage{xspace}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usetheme[white]{wurnewbold2} %blue, green, white, gray

%\usetheme{Frankfurt}
%\usecolortheme{default}
%\useoutertheme[subsection=false]{miniframes}

\renewenvironment{Schunk}{\scriptsize \color{wurgreen}}{} 

\input{/home/ron/tex/style-files/mymath}
\usefonttheme[onlymath]{serif}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{darkgreen}{rgb}{0,.5,.2}
\newcommand{\ok}{{\color{darkgreen} $\surd$}}
\newcommand{\duh}{{\color{red} $\otimes$}}
\newcommand{\soso}{$\Box$}
\newcommand{\code}[1]{{\color{wurgreen} \ttfamily #1}}

%\input{mymath}
\title{R and Big Data\\Big Memory issues}
\author{Ron Wehrens}
\institute{Biometris, Wageningen UR}
\date{Fall 2021}

%% optional lecture-dependent graphic
\renewcommand{\custompic}[1]{data-head}

%%\institute{\color{wurgreen} Biometris / Bioscience}
\begin{document}
\begin{frame}[plain]
  %%  \custompic{rose.png}
  \titlepage
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}[fragile,containsverbatim]
  \frametitle{Big Data?}
  \begin{itemize}[<+->]
  \item Treat yourself! You \emph{deserve} those 128Gb!
  \item Consider bothering others:
    \begin{itemize}
    \item the cluster of thy neighbour
    \item the cloud
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{The Problem}
    \begin{columns}[onlytextwidth]
    \begin{column}{.75\textwidth}
  \begin{Schunk}
\begin{Sinput}
memory.limit() ## This is windows...
\end{Sinput}
\begin{Soutput}
[1] 4095
\end{Soutput}
\begin{Sinput}
a <- matrix(pi, 10000000, 60)  
\end{Sinput}
\begin{Soutput}
Error: cannot allocate vector of size 4.5 Gb  
\end{Soutput}
\end{Schunk}
    \end{column}
    \begin{column}{.25\textwidth}
      \par
      \centerline{
        \includegraphics[width=.6\textwidth]{problem-page.jpg}}
    \end{column}
  \end{columns}
\end{frame}

\section{Memory usage}
\begin{frame}[fragile,containsverbatim]
  \frametitle{Assessing object size}
  \begin{columns}[onlytextwidth]
    \begin{column}[t]{.4\textwidth}
      Basic R function:\\[2ex] \code{object.size}
      \begin{Schunk}
        \begin{Sinput}
object.size(flights)
        \end{Sinput}
        \begin{Soutput}
17236200 bytes
        \end{Soutput}
        \pause
        \begin{Sinput}
format(object.size(flights), 
       units = "auto")
        \end{Sinput}
        \begin{Soutput}
[1] "16.4 Mb"
        \end{Soutput}
      \end{Schunk}

      Approximate!
    \end{column} \pause
    \begin{column}[t]{.5\textwidth}
      All kinds of tweaks can be found on the web:
      
      \begin{Schunk}
        \begin{Sinput}
showMemoryUse(decreasing=TRUE, limit=5)
        \end{Sinput}
        \begin{Soutput}
    objectName memorySize
       coherData  713.75 MB
 spec.pgram_mine  149.63 kB
       stoch.reg  145.88 kB
      describeBy    82.5 kB
      lmBandpass   68.41 kB\end{Soutput}
      \end{Schunk}

      Also:\\ \code{tables()} from
      \pkg{data.table} \\ \code{object\_size()} from \pkg{pryr}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Efficient storage}
  \begin{columns}[onlytextwidth]
    \begin{column}{.45\textwidth}
      Distance objects vs. matrices
      \begin{Schunk}
        \begin{Sinput}
data(wines, package = "kohonen")          
D1 <- dist(wines)
object.size(D1)
        \end{Sinput}
        \begin{Soutput}
126008 bytes
        \end{Soutput}
        \pause
        \begin{Sinput}
D2 <- as.matrix(D1)
object.size(D2)
        \end{Sinput}
        \begin{Soutput}
273776 bytes
        \end{Soutput}
      \end{Schunk}
    \end{column} \pause
    \begin{column}{.45\textwidth}
      Matrices vs. data.frames
      \begin{Schunk}
        \begin{Sinput}
huhn <- matrix(rnorm(9), 3, 3)
object.size(huhn)
        \end{Sinput}
        \begin{Soutput}
[1] 328 bytes
        \end{Soutput} 
        \pause
        \begin{Sinput}
huhndf <- as.data.frame(huhn)
object.size(huhndf)
        \end{Sinput}
        \begin{Soutput}
[1] 992 bytes
        \end{Soutput} 
      \end{Schunk}
        \pause
        vs. vectors
      \begin{Schunk}
        \begin{Sinput}
object.size(c(huhn))
        \end{Sinput}
        \begin{Soutput}
[1] 168 bytes
        \end{Soutput}
      \end{Schunk}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Efficient storage (2)}
  \begin{columns}[onlytextwidth]
    \begin{column}{.45\textwidth}
      Instead of a 100x100 matrix with almost 10,000 zeros, use 
      a sparse representation: \\[2ex]
        \begin{Schunk}
          \begin{Soutput}
     [,1] [,2] [,3]
[1,]    4   85  6.1
[2,]   92   99  9.5
[3,]   15   87  4.2
          \end{Soutput}
        \end{Schunk}

        See the \pkg{Matrix} package.
    \end{column}
    \begin{column}<+->{.45\textwidth}
      \begin{itemize}[<+->]
      \item row and column names take space, too
      \item avoid repeated entries 
        \begin{Schunk}
          \tiny
          \begin{Soutput}
Loc tunnel Code week batch
 AB   TC60  TB1   50     2
 AB   TC41  TB1   47     1
 AB   TC46  TB1   47     1
 AB   TC18  TB1   48     1
 AB   TC39  TB1   52     2
 AB   TC60  TB1   50     2
 AB   TC20  TB1   48     1
 AB   TC20  TB1   48     1
 AB   TC43  TB1   42     1
          \end{Soutput}
        \end{Schunk}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Efficient storage (3) - clever bits}
  \begin{Schunk}
\begin{Sinput}
object.size("banana")
\end{Sinput}
\begin{Soutput}
112 bytes
\end{Soutput}
\begin{Sinput}
object.size(rep("banana", 10))  
\end{Sinput}
\begin{Soutput}
232 bytes
\end{Soutput}
  \end{Schunk}

  \vspace{2ex}

  Same with literal copies of objects\pause

  \vspace{1ex}
  but change one thing and...

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Memory organization in R}
  \begin{columns}[onlytextwidth]
    \begin{column}{.45\textwidth}
      \par
      \centerline{
        \includegraphics[width=.7\textwidth]{messy-dorm-room.jpg}}
    \end{column}
    \begin{column}{.45\textwidth}
      Much like a student room...

      \begin{itemize}
      \item allocated when you need it
      \item only returned it when asked (gingerly):
        \begin{itemize}
        \item removing objects \pause \\
          NOT!
        \item garbage collection
        \end{itemize}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Garbage collection}
\par
\centerline{
  \includegraphics[width=\textwidth]{gcprinciple.png}}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{The \code{gc} function}
  \begin{Schunk}
    \begin{Sinput}
gc()
    \end{Sinput}
    \begin{Soutput}
         used (Mb) gc trigger (Mb) max used (Mb)
Ncells 219121  5.9     460000 12.3   350000  9.4
Vcells 278073  2.2     786432  6.0   691976  5.3
    \end{Soutput}
\end{Schunk}
\begin{itemize}
\item \code{Vcells}: vectors
\item \code{Ncells}: everything else\\(incl overhead for vectors: type,
  length, ...)
\end{itemize}

\vspace{2ex} \pause

No need to use it, though...

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Memory limits}
  \begin{columns}[onlytextwidth]
    \begin{column}{.75\textwidth}
      OS-specific
      
      \vspace{3ex}
      
      \begin{tabular}{lcl} \toprule
        OS & bits & Limit \\ \midrule
        Windows  & 32 & 2 GB \\
        & 64 &  8 TB \\
        Linux/Mac & 32 & 3 GB \\
        & 64 & 128 TB \\ \bottomrule
      \end{tabular}
      
      \vspace{3ex}
      
      Both 64 bits OS \emph{and} 64 bits R needed!

      Sometimes additional restrictions
    \end{column}
    \begin{column}{.25\textwidth}
      \par
      \centerline{
        \includegraphics[width=\textwidth]{featured-operating-systems.png}}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Avoid cluttered workspaces}
  \begin{columns}[onlytextwidth]
    \begin{column}{.45\textwidth}
      Don't like cleaning? \\[2ex]
      Simple: 
      \begin{itemize}
      \item save all your \emph{real} code in a script
      \item run the script in a clean R session
      \end{itemize}

      \pause
      
      \vspace{2ex} Sven is always right!

    \end{column}
    \begin{column}{.45\textwidth}
      \par
      \centerline{
        \includegraphics[width=.6\textwidth]{cleaning.png}}
    \end{column}
  \end{columns}  
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Profiling memory usage}
  \begin{columns}[onlytextwidth]
    \begin{column}[<+->]{.45\textwidth}
      Difficult:
      \begin{itemize}
      \item allocated at well-defined times
      \item freed whenever \code{gc} runs...
      \end{itemize}
    \end{column}
    \begin{column}[<+->]{.45\textwidth}
      \begin{itemize}
      \item \code{Rprof}
%% http://www.hep.by/gnu/r-patched/r-exts/R-exts_73.html#SEC72
      \item tracking memory allocations: \code{Rprofmem}
%% http://www.hep.by/gnu/r-patched/r-exts/R-exts_74.html#SEC74
      \item tracing copies of an object: \code{tracemem}
      \end{itemize}

      \vspace{2ex}
      
      All have their difficulties.\\
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Tracking memory allocation with profmem}
  
  Example: the \pkg{kohonen} package (paper in JSS, 2018)
  
  \par
  \centerline{\includegraphics[width=\textwidth]{memTest-1}}

\end{frame}


\begin{frame}[fragile,containsverbatim]
  \frametitle{Alternatives}
  \begin{columns}[onlytextwidth]
    \begin{column}[t]{.5\textwidth}
      Package \pkg{pryr}:
      \begin{itemize}
      \item \code{\detokenize{mem_used()}}
      \item \code{\detokenize{mem_change()}}
      \end{itemize}

      \vspace{3ex}
      Package \pkg{bench}:
      \begin{itemize}
      \item \code{\detokenize{bench_memory()}}
      \item \code{\detokenize{bench_process_memory()}}
      \end{itemize}
    \end{column}
    \begin{column}[t]{.5\textwidth}
\begin{Schunk}
\begin{Sinput}
mem_change(x <- 1:1e6)
\end{Sinput}
\begin{Soutput}
[1] 3.95 MB
\end{Soutput}
\pause
\begin{Sinput}
mem_change(rm(x))
\end{Sinput}
\begin{Soutput}
[1] -4 MB
\end{Soutput}
\end{Schunk}

    \end{column}
  \end{columns}
\end{frame}

\section{Addressing the problem}
\begin{frame}[fragile,containsverbatim]
  \frametitle{Data too big?}
  Don't keep all data in memory at the same time
  \begin{itemize}[<+->]
  \item \alert{work on a subset at a time}
  \item keep data on disk: \code{bigmemory}
  \item keep data in a database: \code{RSQLite}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Or use your brain...}

  A/B testing:
    \raisebox{-.5\height}{\includegraphics[width=.4\textwidth]{A-B_testing_example.png}}

    \pause

    \vspace{2ex}
  
  \begin{columns}[onlytextwidth]
    \begin{column}[t]{.5\textwidth}
  \par
  \centerline{
    \includegraphics[width=.4\textwidth]{classic.jpg}}
  \begin{Schunk}
    \tiny
    \begin{Sinput}
t.test(mpg ~ am, data = mtcars)
    \end{Sinput}
  \end{Schunk}

    \end{column}
    \pause
    \begin{column}[t]{.5\textwidth}
      Actually, only 6 numbers needed:
      \begin{Schunk}
        \tiny
        \begin{Sinput}
welchTest <- function(mns, vrs, ns) {
  tVal <- diff(mns) / sqrt(sum(vrs/ns))

  df    <- ((sum(vrs/ns))^2) /
            sum(vrs^2/(ns^2 * (ns - 1)))

  pVal <- 2 * pt(abs(tVal), df, lower = FALSE)

  data.frame(tVal = tVal,
             df    = df,
             pVal = pVal)
}
\end{Sinput}
\end{Schunk}
    \end{column}
  \end{columns}
  
\end{frame}

%% \begin{frame}[fragile,containsverbatim]
%%   \frametitle{Take five!}
%% \par
%% \centerline{
%% \includegraphics[width=.6\textwidth]{takefive.jpg}}
%% \end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{The \pkg{bigmemory} package}
  \begin{columns}[onlytextwidth]
    \begin{column}<+->{.45\textwidth}
      \begin{itemize}[<+->]
      \item represent large objects by pointers to
        C++ structures
      \item in shared memory - can also be shared by different R processes
      \item does not show in \code{gc()}!
      \item optional: file-backing on disk - objects may be
        \emph{much} larger than your RAM...
      \end{itemize}
    \end{column}
    \begin{column}<+->{.45\textwidth}
      Alternatives:
      \begin{itemize}
      \item \code{ff}
      \item \code{filehash}
      \item \code{SOAR}
      \item \code{R.huge}
      \item \code{Rhadoop}
      \item ...
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Properties of \code{bigmemory}}
  \begin{columns}[onlytextwidth]
    \begin{column}{.45\textwidth}
      \begin{itemize}
      \item matrices only
      \item base functions (e.g., \code{kmeans}, \code{lm}) do not work
      \item since version \code{4.0} split over several packages:
        \code{bigmemory}, \code{biganalytics}, \code{bigalgebra},
        \code{bigtabulate}, \code{synchronicity} 
      \item excellent integration with parallel 
        computing using \code{foreach}
      \end{itemize}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{Schunk}
        \begin{Sinput} 
object.size(flights)
        \end{Sinput}
        \begin{Soutput}
[1] 17448304 bytes
        \end{Soutput} 
        \pause
        \begin{Sinput}
flights.bm <- 
  as.big.matrix(flights)
object.size(flights.bm)
        \end{Sinput} 
        \pause
        \begin{Soutput}
[1] 664 bytes
        \end{Soutput}
        \pause
        \begin{Sinput}
dim(flights.bm)
        \end{Sinput}
        \begin{Soutput}
[1] 253317     17
        \end{Soutput}
      \end{Schunk}
    \end{column}
  \end{columns}  
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Analysis for big data sets}
  \begin{columns}[onlytextwidth]
    \begin{column}<+->[t]{.45\textwidth}
      The \pkg{biglm} package (Lumley):
      \begin{itemize}[<+->]
      \item create a linear model using only $O(p^2)$ memory for $p$
        variables
      \item add more data using \code{update}
      \item in this way data sets larger than memory can be handled!
      \item \code{bigmemory} presents a variant for \code{big.matrix} objects
      \end{itemize}
    \end{column}
    \begin{column}<+->[t]{.45\textwidth}
      In the \pkg{biganalytics} package, several functions, \emph{e.g.},
      \code{bigkmeans}:
      \begin{itemize}[<+->]
      \item no memory overhead: just the data matrix and the class
        labels
      \item classical \code{kmeans} at least two extra copies
      \item also works with ``normal'' matrices
      \end{itemize}
    \end{column}
  \end{columns} 
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Your turn! The \code{flights} data again.}

  \begin{Schunk}
    \tiny
    \begin{Sinput}
library(data.table)
input <- "https://raw.githubusercontent.com/Rdatatable/data.table/master/vignettes/flights14.csv"
flights <- fread(input)
    \end{Sinput}
  \end{Schunk}

  \begin{itemize}
  \item compare the speed of selecting a subset\\(say, flights from JFK
    in Feb and Mar) for
    \begin{itemize}
    \item the original \code{data.frame}
    \item the \code{data.table} form
    \item \code{bigmemory} without file backing
    \item \code{bigmemory} with file backing
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Results (on my laptop)}
  \begin{Schunk} \tiny
\begin{Sinput}
## select only flights from JFK between Feb 1 amnd March 31
flights.df <- as.data.frame(flights)
system.time(for (i in 1:10)
              huhn <- flights.df[flights.df$month %in% 2:3 &
                                   flights.df$origin == "JFK",])
##  user  system elapsed 
## 0.112   0.008   0.120 

\end{Sinput}
  \end{Schunk} \pause
  \begin{Schunk} \tiny
\begin{Sinput}
system.time(for (i in 1:100)
              huhn <- flights[month %in% 2:3 & origin == "JFK",])
##   user  system elapsed 
##  0.326   0.008   0.334 
\end{Sinput}
\end{Schunk} \pause
  \begin{Schunk} \tiny
\begin{Sinput}
flights.bm <- as.big.matrix(flights)
## NOTE: all non-numerical columns are converted to factors to integers
## You CANNOT use "JFK" in the origin
system.time(for (i in 1:10)
              huhn <- flights.bm[flights.bm[,"month"] %in% 2:3 &
                                   flights.bm[,"origin"] == 2,])
##   user  system elapsed 
##  1.294   0.028   1.325
\end{Sinput}
\end{Schunk} \pause
  \begin{Schunk} \tiny
\begin{Sinput}
flights.bm2 <- as.big.matrix(flights, backingfile = "fl14TMP")
system.time(for (i in 1:10)
              huhn <- flights.bm2[flights.bm2[,"month"] %in% 2:3 &
                                    flights.bm2[,"origin"] == 2,])
##   user  system elapsed 
##  1.079   0.012   1.091 
\end{Sinput}
\end{Schunk}
\pause
  Look at \code{?mwhich} for fast selection
\end{frame}


\begin{frame}[fragile,containsverbatim]
  \frametitle{Big K Means}
  \begin{itemize}
  \item Perform a k-means clustering on the time-related columns of the
    flight data, using the \code{bigkmeans} function. Choose any $k$
    that you like...
  \item Compare the results with the results of the regular
    \code{kmeans} function in terms of computing resources.
  \item Are the clusters related to,
    \emph{e.g.}, season? Is the clustering meaningful, anyway?
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Timing results (on my laptop)}
  \begin{Schunk}
    \begin{Sinput}
library(biganalytics)

mymat <- flights[,c("dep_delay", "arr_delay", "air_time", "hour")]
mymat <- scale(mymat) ## discuss!
mymat.df <- as.data.frame(mymat)
mymat.bm <- as.big.matrix(mymat)
mymat.bm2 <- as.big.matrix(mymat, backingfile = "bmTMP")
    \end{Sinput} 
  \end{Schunk}\pause
  \begin{Schunk}
    \begin{Sinput}
system.time(huhn <- kmeans(mymat, centers = 6))
##  user  system elapsed 
## 0.363   0.008   0.373 
    \end{Sinput}
  \end{Schunk} \pause
  \begin{Schunk}
    \begin{Sinput}
system.time(huhn <- kmeans(mymat.df, centers = 6))
##  user  system elapsed 
## 0.371   0.004   0.376 
    \end{Sinput}
  \end{Schunk} \pause
  \begin{Schunk}
    \begin{Sinput}
system.time(huhn <- bigkmeans(mymat.bm, centers = 6))
##  user  system elapsed  ## no convergence in 10 its
## 0.184   0.000   0.184 
    \end{Sinput}
  \end{Schunk} \pause
  \begin{Schunk}
    \begin{Sinput}
system.time(huhn <- bigkmeans(mymat.bm2, centers = 6))
##   user  system elapsed 
##  0.155   0.000   0.155 
    \end{Sinput}
  \end{Schunk}

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{... and memory consumption}
  \begin{Schunk}
    \begin{Sinput}
require(profmem)      
total(profmem(huhn <- kmeans(mymat, centers = 6)))
## [1] 53713896
    \end{Sinput}
  \end{Schunk} \pause
  \begin{Schunk}
    \begin{Sinput}
total(profmem(huhn <- kmeans(mymat.df, centers = 6)))
## [1] 41548232
    \end{Sinput}
  \end{Schunk} \pause
  \begin{Schunk}
    \begin{Sinput}
total(profmem(huhn <- bigkmeans(mymat.bm, centers = 6)))
## [1] 6322760
    \end{Sinput}
  \end{Schunk} \pause
  \begin{Schunk}
    \begin{Sinput}
total(profmem(huhn <- bigkmeans(mymat.bm2, centers = 6)))
## [1] 6327864
    \end{Sinput}
  \end{Schunk}

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Function biglm}
  \begin{itemize}
  \item Compare \code{biglm} and \code{lm} regression models in terms
    of time and memory use: fit, \emph{e.g.}, \code{arr\_delay} as a
    function of \code{distance}.
  \item Suppose \code{flights} is too big for your computer. Fit a
    \code{biglm} model on the first half, and then update your model
    with the second half. Compare the results with the one-pass model.
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Biglm results}
\begin{Schunk}\scriptsize
\begin{Sinput}
total(profmem(lmmod1 <- lm(arr_delay ~ distance,
                           data = flights.df)))
## [1] 46161800
total(profmem(lmmod2 <- biglm(arr_delay ~ distance,
                              data = flights.df)))
## [1] 39661440
total(profmem(lmmod3 <- biglm.big.matrix(arr_delay ~ distance,
                                         data = flights.bm)))
## [1] 83189688
\end{Sinput}
\end{Schunk} \pause

\begin{Schunk}\scriptsize
  \begin{Sinput}
N <- 150000
lmmod4 <- biglm(arr_delay ~ distance,
                data = flights.df[1:N,])
lmmod4 <- update(lmmod4,
                 moredata = flights.df[(N+1):nrow(flights.df),])
  \end{Sinput}
\end{Schunk} \pause
\begin{Schunk} \scriptsize
  \begin{Sinput} 
coef(lmmod2)
 (Intercept)     distance 
 9.703689724 -0.001416157 
coef(lmmod4)
 (Intercept)     distance 
 9.703689724 -0.001416157 
 \end{Sinput}
\end{Schunk}
\end{frame}


\begin{frame}[fragile,containsverbatim]
  \frametitle{Caveats and remarks}
  \begin{itemize}[<+->]
  \item \code{big.matrix} as a function argument: call-by-reference
    behaviour
    \begin{itemize}
    \item changing a matrix element will lead to a global change!
    \item same as with \code{data.table}
    \item \alert{RED} alert...
    \end{itemize}
  \item \code{data.frames} are coerced to matrices - characters will
    become factors will become integers...
  \item Maximum object size: your hard drive
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Databases}
  \begin{itemize}
  \item fast access to selected parts
  \item summaries and cross-tables
  \item more organized storage
  \item concurrent access
  \item server capabilities
  \end{itemize}

  \vspace{3ex}

  {\color{gray}\small From the R manual on CRAN...}
\end{frame}


\begin{frame}[fragile,containsverbatim]
  \frametitle{The \pkg{RSQLite} package}
  \begin{Schunk}
    \begin{Sinput}
library(DBI)  # includes RSQLite backend
## create a new database
mydb <- dbConnect(RSQLite::SQLite(), "my-db.sqlite")
## write data to DB
dbWriteTable(mydb, "mtcars", mtcars)
dbWriteTable(mydb, "iris", iris)
dbListTables(mydb)
\end{Sinput}
\begin{Soutput}
[1] "iris"   "mtcars"
\end{Soutput}
\begin{Sinput}
dbDisconnect(mydb)
unlink("my-db.sqlite")
\end{Sinput}
  \end{Schunk}

  \pause
  
  \begin{itemize}
  \item select subsets from whole in \code{SQLite} to be
    processed in \code{R} (cf. the A-B testing example)
  \item send R code to the database
  \end{itemize}
\end{frame}


\section{}
\begin{frame}[fragile,containsverbatim]
  \frametitle{Conclusions}
  \begin{itemize}[<+->]
  \item Develop your scripts using small (but representative!) subsets 
  \item Big data need not be a problem: the full flights data set is
    12GB!
  \item You always pay a price, usually speed
  \item Fight back using parallel processing!
  \item There are many, many ways to achieve similar results
  \end{itemize}
\end{frame}


\begin{frame}[fragile,containsverbatim]
  \frametitle{Sources}
  \begin{itemize}
    \item Hadley Wickham: ``Advanced R'', Chapman and
      Hall, \url{adv-r.had.co.nz}
    \item Manual ``Writing R extensions''\\
      \url{cran.r-project.org/doc/manuals/r-release/R-exts.html}
    \item \url{www.r-bloggers.com}
    \item Vignettes for the \pkg{bigmemory} package
  \end{itemize}
\end{frame}

\end{document}
