\documentclass[fleqn]{beamer}
\mode<presentation>
%% \mode<handout>

\usepackage{amsmath, mathtools}
\usepackage{booktabs}
\usepackage{moresize}
\usepackage{wasysym}
\usepackage{verbatim}
\usepackage[nogin]{Sweave}          % for R input and output
\usepackage{comment, latexsym}
\usepackage{xspace}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usetheme[white]{wurnewbold2} %blue, green, white, gray

%\usetheme{Frankfurt}
%\usecolortheme{default}
%\useoutertheme[subsection=false]{miniframes}

%% needed packages: ggplot2, ggbeeswarm, bench, Rprof, proftools,
%% profvis, data.table, 

\renewenvironment{Schunk}{\small \color{wurgreen}}{} 

\input{/home/ron/tex/style-files/mymath}
\usefonttheme[onlymath]{serif}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{darkgreen}{rgb}{0,.5,.2}
\newcommand{\ok}{{\color{darkgreen} $\surd$}}
\newcommand{\duh}{{\color{red} $\otimes$}}
\newcommand{\soso}{$\Box$}
\newcommand{\code}[1]{{\color{wurgreen} \ttfamily #1}}

%\input{mymath}
\title{R and Big Data\\Profiling and Efficient Programming}
\author{Ron Wehrens}
\institute{Biometris, Wageningen UR}
\date{Fall 2021}

%% optional lecture-dependent graphic
\renewcommand{\custompic}[1]{data-head}

%%\institute{\color{wurgreen} Biometris / Bioscience}
\begin{document}
\begin{frame}[plain]
  %%  \custompic{rose.png}
  \titlepage
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}[fragile,containsverbatim]
  \frametitle{Before we start...}
\begin{quote}
Programmers waste enormous amounts of time thinking about, or worrying
about, the speed of noncritical parts of their programs, and these
attempts at efficiency actually have a strong negative impact when
debugging and maintenance are considered. \hfill - Donald Knuth
\end{quote}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Optimizing Code}
  \begin{enumerate}
  \item Find the bottleneck (part 1)
  \item Eliminate it (part 2)
  \item Go to 1
  \end{enumerate}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Relevant differences?}
    \begin{columns}[onlytextwidth]
    \begin{column}{.5\textwidth}
      ``Live'' computing
      \begin{itemize}
      \item .01 secs rather than .05 secs
      \item 1 sec rather than 5 secs
      \item 1 min rather than 5 mins
      \item 1 hour rather than 5 hours
      \end{itemize}
    \end{column} \pause
    \begin{column}{.5\textwidth}
      Batch computing
      \begin{itemize}
      \item .01 secs rather than .05 secs
      \item 1 s rather than 5 s
      \item 1 min rather than 5 min
      \item 1 hour rather than 5 hours
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Disclaimer}
    \begin{columns}[onlytextwidth]
    \begin{column}{.5\textwidth}
  \begin{itemize}
  \item only baseR, with packages added
  \item \emph{not} tidyverse
  \item one exception: \code{data.table}
  \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
      \par
      \centerline{
        \includegraphics[width=.6\textwidth]{disclaimerpng}}

    \end{column}
  \end{columns}
\end{frame}

\section{Bottles and necks}
\begin{frame}[fragile,containsverbatim]
  \frametitle{\code{system.time()}}
  Simplest possible approach: assess execution time of a function

\begin{Schunk}
\begin{Sinput}
system.time(rnorm(1e6))
\end{Sinput}
\begin{Soutput}
   user  system elapsed 
  0.064   0.000   0.064 
\end{Soutput}
\end{Schunk}

Simply takes difference in \code{proc.time} before and after

\pause

{\scriptsize
\begin{itemize}
\item \code{user}: current process (here: R session)
\item \code{system}: kernel (shared resources, e.g., I/O) on behalf of
  the current process 
\end{itemize}
}

\pause

Alternative: package \code{tictoc}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Example from plotting}
    \begin{columns}[onlytextwidth]
    \begin{column}{.5\textwidth}
      \par
      \centerline{
        \includegraphics[width=.6\textwidth]{bigSet.png}}
\begin{Schunk}
\begin{Sinput}
system.time(
  plot(huhn)
)
\end{Sinput}
\begin{Soutput}
   user  system elapsed 
  7.343   0.016   7.361 
\end{Soutput}
\end{Schunk}
    \end{column} \pause
    \begin{column}{.5\textwidth}
      \par
      \centerline{
        \includegraphics[width=.6\textwidth]{bigSet2.png}}
\begin{Schunk}
\begin{Sinput}
system.time(
  plot(huhn, pch = ".", cex = 3)
)
\end{Sinput}
\begin{Soutput}
   user  system elapsed 
  0.329   0.000   0.330 
\end{Soutput}
\end{Schunk}

    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{\code{system.time()} considerations}
  \begin{itemize}[<+->]
  \item result is variable - use multiple times
  \item depends on load, etcetera... - relative measure
  \item function too fast: simply time repeated version
  \item hard to compare multiple versions - use benchmarking instead!
  \item wrapper around \code{system.time()}: package \code{rbenchmark}
  \item more elaborate option: \code{microbenchmark}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Even better benchmarking}
%% \begin{Schunk}
%% \scriptsize
%% \begin{Sinput}
%% require(microbenchmark)
%% compare <- microbenchmark(f(1000, 1), g(1000, 1), times = 1000)

%% require(ggplot2)
%% autoplot(compare)
%% \end{Sinput}
%% \end{Schunk}
\begin{Schunk}
\scriptsize
\begin{Sinput}
require(bench)
compare <- bench::mark(f1(rnorm(2000)), f2(rnorm(2000)))

require(ggplot2)
autoplot(compare)
\end{Sinput}
\end{Schunk}

\par
\centerline{
\includegraphics[width=8cm]{benchmark.png}}

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Get active!}
  Task 1: compare the speed of \code{mean(x)} with \code{sum(x) /
    length(x)} for different lengths of \code{x}. And what about
  \code{mean.default}?

\vspace{2ex}

\pause

Task 2: compare the speed of selecting one column in a \code{matrix} and
in a \code{data.frame} object.

\begin{itemize}
\item Does the difference depend on the object size?
\item Do the same for the selection of a row. What do you notice?
  
\end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Profiling}
  Measure at regular intervals what R function is busy\\[2ex]

  Higher level than benchmarking single functions\\[2ex]

  Workhorse: \code{Rprof}
  \begin{enumerate}
  \item \code{summaryRprof}
  \item static visualization with the \code{proftools} package
  \item interactive visualization with the \code{profvis} package (included in RStudio)
  \end{enumerate}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Rprof example}
  \begin{Schunk}
    \scriptsize
    \begin{Sinput}
require(kohonen)
data(peppaPic)      
Rprof("rprof.out")
mymap <- som(peppaPic[,-1], somgrid(8, 4, "hexagonal"))
Rprof(NULL)
huhn <- summaryRprof("rprof.out")
huhn$by.total[1:10,]
    \end{Sinput}
    \pause
    \begin{Soutput}
                total.time total.pct self.time self.pct
"som"                26.22    100.00      0.00     0.00
"supersom"           26.22    100.00      0.00     0.00
".Call"              23.50     89.63     23.50    89.63
"RcppSupersom"       23.38     89.17      0.00     0.00
"FUN"                 2.62      9.99      0.54     2.06
"lapply"              2.62      9.99      0.02     0.08
"apply"               2.56      9.76      1.54     5.87
"check.data.na"       1.72      6.56      0.00     0.00
"which"               1.72      6.56      0.00     0.00
"map.kohonen"         0.98      3.74      0.00     0.00
    \end{Soutput}
  \end{Schunk}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Visualization with the \code{proftools} package}
\begin{Schunk}
    \scriptsize
\begin{Sinput}
woppa <- readProfileData("rprof.out")
plotProfileCallGraph(woppa, score = "total", style = plain.style)
\end{Sinput}
\end{Schunk}
  
  \par
  %% \centerline{
  %%   \fbox{\includegraphics[width=.75\textwidth, clip = TRUE, trim = 10cm 13cm 0cm 0cm]{RprofOutput}}}
  %% \par
  \centerline{
    \fbox{\includegraphics[width=.75\textwidth, clip = TRUE, trim = 0cm 6cm 0cm 0cm]{rprofoutput2}}}
\end{frame}


\begin{frame}[fragile,containsverbatim]
  \frametitle{Using profvis}
\begin{Schunk}
  \begin{Sinput}
profvis(prof_input ="rprof.out")    
\end{Sinput}
\end{Schunk}
\par
\centerline{
  \includegraphics[width=.7\textwidth]{profvisScreenshot.png}}

  \pause

  Or type directly:
  
  \begin{Schunk}

  \begin{Sinput}
profvis(som(peppaPic[,-1], somgrid(8, 4, "hexagonal")))
  \end{Sinput}
\end{Schunk}
  
\end{frame}

\section{Efficient programming}
\begin{frame}[fragile,containsverbatim]
  \frametitle{Efficent programming: caveats}
  \begin{itemize}[<+->]
  \item readability
  \item transferabilty
  \item don't you have better things to do?
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Optimize your code}
  \begin{itemize}[<+->]
  \item \emph{use your brain!}
  \item OK, OK, ... use google
  \item use internal R functions as much as possible
  \item use specialized functions
  \item vectorize functions
  \item avoid copies of objects
  \item use compiled code (C, C++, Fortran, ...)
  \item parallelize (lecture Sven)
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Use your brain}
  Example: PCA
\[\bX = \bU \bD \bV^T = \bT \bP^T \]
  
  \begin{columns}[onlytextwidth]
    \begin{column}[t]{.5\textwidth}
      Classical:
      \begin{Schunk}
      \tiny
        \begin{Sinput}
system.time({
  p.svd <- svd(prostate)
  p.scores <- 
    p.svd$u %*% diag(p.svd$d)
  p.variances <- 
    p.svd$d^2 / (nrow(prostate) - 1)
  p.loadings <- p.svd$v
})
\end{Sinput}
\begin{Soutput}
   user  system elapsed
 30.706   0.264  30.988
\end{Soutput}
\end{Schunk}
    \end{column} \pause
    \begin{column}[t]{.5\textwidth}
      Alternative (for fat matrices):
\begin{Schunk}
      \tiny
\begin{Sinput}
system.time({
  p.tcp <- tcrossprod(prostate)
  p.svd <- svd(p.tcp)
  p.scores <- 
    p.svd$u %*% diag(sqrt(p.svd$d))
  p.variances <- 
    p.svd$d / (nrow(prostate) - 1)
  p.loadings <- solve(p.scores, prostate)
  })
\end{Sinput}
\begin{Soutput}
   user  system elapsed
 16.085   0.132  16.216
\end{Soutput}
\end{Schunk} 
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Use your brain, part 2}

%  \code{any(x == 2)} is faster than \code{1 \%in\% x}

%    \pause

%    \vspace{3ex}
  
  Which is faster?
  \begin{Schunk}
\begin{Sinput}
A %*% B %*% y
\end{Sinput}
\begin{Sinput}
A %*% (B %*% y)
\end{Sinput}
\end{Schunk}
\end{frame} 

\begin{frame}[fragile,containsverbatim]
  \frametitle{Use specialized functions}
\begin{Schunk}
\begin{Sinput}
crossprod(X) ## faster than
t(X) %*% X
crossprod(X, y) ## faster than
t(X) %*% y
tcrossprod(X, y) ## faster than
X %*% t(y)
tcrossprod(v, m) ## faster than
crossprod(v, t(m))
\end{Sinput}
\end{Schunk}

\end{frame}


\begin{frame}[fragile,containsverbatim]
  \frametitle{Vectorize!}
  Example: compare two (equal-length) vectors of integers, \code{x} and \code{y}

  Brainless version:
  \begin{Schunk}
    \begin{Sinput}
result <- 0
for (ii in 1:length(x))
    if (x[ii] == y[ii]) result <- result + 1
    \end{Sinput}
  \end{Schunk}

  \pause
  Alternative:
  \begin{Schunk}
    \begin{Sinput}
result <- sum(x == y)          
    \end{Sinput}
  \end{Schunk}

  \pause
  
  Be careful when comparing doubles!
  \begin{Schunk}
    \begin{Sinput}
sum(abs(x - y) < 1e-8)
    \end{Sinput}
  \end{Schunk}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Case study: cross validation}
\begin{Schunk}
\begin{Sinput}
require(pls)
example(plsr)
r1 <- yarn$density - yarn.pls$validation$pred[,1,1]
r2 <- yarn$density - yarn.pls$validation$pred[,1,2]
\end{Sinput}
\end{Schunk}

  
\par
\centerline{
\includegraphics[width=8cm]{plsResiduals}}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Permutation test}
  Central idea: if two groups are equal, then we can shuffle without
  seeing any effect (no assumptions needed)

  \pause
  
  \begin{enumerate}
  \item assign data points randomly to one of the two groups
  \item calculate statistics of interest
  \item go to 1 until you have had enough
  \item {\color{gray}plot the null distribution, add the sample statistic}
  \item Q: how often is the null bigger than the sample statistic?
  \end{enumerate}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Spot the differences (10 mins)}
\begin{Schunk}
\scriptsize
\begin{Sinput}
rf1 <- function(rnew, rref, nPerm) {
  d <- rnew^2 - rref^2; N <- length(d); count <- 0
  for (i in 1:nPerm) {
    sign <- round(runif(N))*2 - 1 ## vector of +1 or -1 values
    if (mean(d * sign) > mean(d))
    count <- count + 1
  }
  (count + .5) / (nPerm + 1)
}
\end{Sinput}
\end{Schunk}
  \begin{Schunk}
\scriptsize
    \begin{Sinput}
rf2 <- function(rnew, rref, nperm) {
  d <- rnew^2 - rref^2; md <- mean(d); N <- length(d)
  signs <- round(matrix(runif(N * nperm), N, nperm)) * 2 - 1
  dsigns <- d * signs
  mdsigns <- colMeans(dsigns)
  count <- sum(mdsigns >= md)
  (count + .5) / (nperm + 1)
}
    \end{Sinput}
  \end{Schunk}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{If you cannot vectorize...}

  You have to use loops: \code{for}, \code{apply} and friends

  \pause
  
  Myth alert! \code{lapply} and \code{sapply} are not faster than
  \code{for}\\[3ex] \pause

  \begin{columns}[onlytextwidth]
    \begin{column}[t]{.5\textwidth}
      Reasons to use \code{lapply}/\code{sapply}:
      \begin{itemize}
      \item simple bits of code: good readability
      \item no memory pre-allocation necessary
      \end{itemize}
    \end{column} \pause
    \begin{column}[t]{.5\textwidth}
      Reasons for \code{for}-loops:
      \begin{itemize}
      \item complicated things
      \item result inserted in already existing data structure
      \end{itemize}
    \end{column}
  \end{columns} \pause

  \vspace{2ex}

  \code{vapply} \emph{is} faster than \code{sapply}, though...
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Avoid copies of objects}
  Example: continuously growing objects (from the ``R Inferno'')
  \begin{columns}[onlytextwidth]
    \begin{column}{.5\textwidth}
\begin{Schunk}
\scriptsize
\begin{Sinput}
f1 <- function(n) {
  vec <- numeric(0)
  for (i in 1:n) vec <- c(vec, i)
  vec
}

f2 <- function(n) {
  vec <- numeric(n)
  for (i in 1:n) vec[i] <- i
  vec
}

f3 <- function(n) {
  1:n
}
\end{Sinput}
\end{Schunk}
    \end{column} \pause
    \begin{column}{.5\textwidth}
\scriptsize
\begin{tabular}{rccc} \hline
n & f1 & f2 & f3 \\ \hline
1,000 & 0.01 & 0.01 & 0.00006 \\
10,000 & 0.59 & 0.09 & 0.0004 \\
100,000 & 133.68 & 0.79 & 0.005 \\
1,000,000 & 18,718 & 8.10 & 0.97 \\ \hline
\end{tabular}
    \end{column}
  \end{columns}

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{The data.table package (Matt Dowle)}
  A \code{data.table} is an enhanced \code{data.frame} 

  \vspace{4mm}
  
  \begin{columns}[onlytextwidth]
    \begin{column}{.5\textwidth}
      The good:
      \begin{itemize}
      \item very fast indexing and more
      \item handy functions for console analysis
      \item reference semantics
      \end{itemize}
    \end{column} \pause
    \begin{column}{.5\textwidth}
      The ugly:
      \begin{itemize}
      \item unusual R construct
      \item \code{data.table} objects look \emph{a lot} like
        \code{data.frame} objects
      \item $P(\mbox{bugs}) \leadsto 1$
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Indexing \code{data.table} objects}
\begin{Schunk}
\begin{Sinput}
require(data.table)
input <- "https://raw.githubusercontent.com/Rdatatable/
          data.table/master/vignettes/flights14.csv"
flights <- fread(input)
DT <- flights
DF <- as.data.frame(flights)
\end{Sinput}
\end{Schunk}

\pause

\begin{columns}[onlytextwidth]
  \begin{column}{.5\textwidth}
    \begin{Schunk}
      \begin{Sinput}
system.time(
DF[DF$origin == "JFK" & 
   DF$month == 6L,]
)
      \end{Sinput}
      \begin{Soutput}
user  system elapsed 
0.018   0.003   0.021 
      \end{Soutput}
\end{Schunk}
    \end{column} \pause
    \begin{column}{.5\textwidth}
\begin{Schunk}
  \begin{Sinput}
system.time(
  DT[origin == "JFK" & 
     month == 6L]
)
  \end{Sinput}
  \begin{Soutput}
 user  system elapsed 
0.012   0.000   0.010
  \end{Soutput}
\end{Schunk}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{General \code{data.table} syntax}
  \begin{columns}[onlytextwidth]
    \begin{column}{.4\textwidth}
      \code{DT[i, j, by]}
        
        \begin{itemize}
        \item[\code{i}] subset and sort \emph{rows}
        \item[\code{j}] select, and compute on, \emph{columns}
        \item[\code{by}] group columns
        \end{itemize}
        
    \end{column} \hspace{5mm} \pause
    \begin{column}{.6\textwidth}
      \begin{overlayarea}{\textwidth}{.8\textheight}
        \begin{onlyenv}<2>
          \begin{Schunk}
            \ssmall
            \begin{Sinput}
flights[1:5, .(arr_delay, dep_delay)]
            \end{Sinput}
            \begin{Soutput}
   arr_delay dep_delay
1:        13        14
2:        13        -3
3:         9         2
4:       -26        -8
5:         1         2
            \end{Soutput}
          \end{Schunk}
        \end{onlyenv}
        \begin{onlyenv}<3>
          \begin{Schunk}
            \ssmall
            \begin{Sinput}
flights[order(dep_delay), 
        .(arr_delay, dep_delay)]
            \end{Sinput}
            \begin{Soutput}
        arr_delay dep_delay
     1:      -112      -112
     2:       -40       -34
     3:        18       -27
     4:       -42       -27
     5:       -24       -25
    ---                    
253312:      1115      1056
253313:      1064      1071
253314:      1090      1087
253315:      1223      1241
253316:      1494      1498
            \end{Soutput}
          \end{Schunk}
        \end{onlyenv}
        \begin{onlyenv}<4>
          \begin{Schunk}
            \ssmall
            \begin{Sinput}
flights[, sum((arr_delay + dep_delay) > 100)]
            \end{Sinput}
            \begin{Soutput}
[1] 23521
            \end{Soutput}
            \begin{Sinput}
flights[(arr_delay + dep_delay) > 100, .N]
            \end{Sinput}
            \begin{Soutput}
[1] 23521
            \end{Soutput}
          \end{Schunk}
        \end{onlyenv}
        \begin{onlyenv}<5>
          \begin{Schunk}
            \ssmall
            \begin{Sinput}
flights[origin == "JFK", 
        .(mean(arr_delay), 
          mean(dep_delay))]
            \end{Sinput}
            \begin{Soutput}
         V1       V2
1: 7.731465 11.44617
            \end{Soutput}
          \end{Schunk}
        \end{onlyenv}
        \begin{onlyenv}<6>
          \begin{Schunk}
            \ssmall
            \begin{Sinput}
flights[carrier == "AA", .N, by = origin]
            \end{Sinput}
            \begin{Soutput}
   origin     N
1:    JFK 11923
2:    LGA 11730
3:    EWR  2649
            \end{Soutput}
          \end{Schunk}
        \end{onlyenv}
        \begin{onlyenv}<7>
          \begin{Schunk}
            \ssmall
            \begin{Sinput}
flights[, .N, 
        by = .(dep_delay > 0, 
               arr_delay > 0)]
            \end{Sinput}
            \begin{Soutput}
   dep_delay arr_delay      N
1:      TRUE      TRUE  72836
2:     FALSE      TRUE  34583
3:     FALSE     FALSE 119304
4:      TRUE     FALSE  26593
            \end{Soutput}
          \end{Schunk}
        \end{onlyenv}
      \end{overlayarea}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Diff summary: \code{data.table} and \code{data.frame}}
  \code{data.table}:
  \begin{itemize}[<+->]
  \item no row names
  \item columns can be referred to directly by name
  \item commas are used differently
  \item new operators (the dot, \code{.N})
  \item new functions (faster sort, for instance)
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{So why all the fuss?}
  \begin{columns}[onlytextwidth]
    \begin{column}[t]{.5\textwidth}
      R normally operates on copies of objects:
      \begin{Schunk}
        \begin{Sinput}
x[3:5] <- 13:15
        \end{Sinput}
      \end{Schunk}
      is actually
      \begin{Schunk}
        \begin{Sinput}
`*tmp*` <- x
x <- "[<-"(`*tmp*`, 
           3:5, 
           value=13:15)
rm(`*tmp*`)
        \end{Sinput}
      \end{Schunk}
      Bad for \emph{large} objects...
    \end{column} \pause
    \begin{column}[t]{.5\textwidth}
      \begin{itemize}
      \item \code{data.table} objects support the \code{:=} operator
      \item no copies: faster, sometimes much faster
      \item similar to using pointers in C
      \item \alert{dangerous!}
      \end{itemize}

      \vspace{2ex}

      Note: already large performance improvements in R v3.1 because of
      shallow copying
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Summary: \code{data.table}}
    \begin{columns}[onlytextwidth]
    \begin{column}{.5\textwidth}
      Very useful, but not for everyone...
    \end{column}
    \begin{column}{.5\textwidth}
      \par
      \centerline{
        \includegraphics[width=.6\textwidth]{acquired.jpg}}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Remember this one?}
Task 2: compare the speed of selecting one column in a \code{matrix} and
in a \code{data.frame} object.

\begin{itemize}
\item Does the difference depend on the object size?
\item Do the same for the selection of a row. What do you notice?
\end{itemize}

\pause
\begin{Schunk}
  \begin{Sinput}
input <- "https://raw.githubusercontent.com/Rdatatable/
          data.table/master/vignettes/flights14.csv"
flights <- fread(input)
\end{Sinput}
\end{Schunk}

Now: use the \code{flights} data, and compare the speed of selecting
rows and columns in both \code{data.table} and \code{data.frame} formats.

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Get active, again!}
  Find the distribution of the determinant of a 2x2 matrix where the
  entries are independent and uniformly distributed digits \\(so find
  all ac - bd values (a, b, c, d: digits) and their frequencies):

  \begin{columns}[onlytextwidth]
    \begin{column}{.5\textwidth}
  \begin{Schunk}
    \scriptsize
    \begin{Sinput}
dd.for.c <- function() {
  val <- NULL

  for (a in 0:9)
    for (b in 0:9)
      for (d in 0:9)
        for (e in 0:9)
          val <- c(val, a*b - d*e)

  table(val)
}
    \end{Sinput}
  \end{Schunk}
  (From the S programming book)
    \end{column} \pause
    \begin{column}{.5\textwidth}
      \par
      \centerline{
        \includegraphics[width=\textwidth]{determinant}}
    \end{column}
  \end{columns}
  
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{One better (at least faster) solution}
      \begin{Schunk}
        \scriptsize
        \begin{Sinput}
dd.for.c2 <- function() {
  val <- outer(0:9, 0:9, "*")
  val2 <- outer(val, val, "-")
  tabulate(val2 + 82)
}
        \end{Sinput}
      \end{Schunk}
      \pause
      \begin{Schunk}
        \scriptsize
        \begin{Sinput}
compare <- bench::mark(dd.for.c(), dd.for.c2(),
                       iterations = 100, check=FALSE)
autoplot(compare)
        \end{Sinput}
      \end{Schunk}
      \par
      \centerline{
        \includegraphics[width=.5\textwidth]{ddforcBench.png}}

\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Use other (compiled) languages}
  \begin{itemize}[<+->]
  \item Foreign language interfaces: \code{.C}, \code{.C++}, \code{.Fortran}
    \begin{itemize}
    \item \emph{Big} performance boosts
    \item need a second language
    \item package: easy
    \item your own scripts: not so easy
    \end{itemize}
  \item In-line compilation: the \code{Rcpp} and \code{inline} packages
    \begin{itemize}
    \item \emph{Big} performance boosts
    \item need a second language
    \item easy in packages as well as scripts
    \end{itemize}
  \end{itemize}
\end{frame}

\section{}
\begin{frame}[fragile,containsverbatim]
  \frametitle{Conclusions}
  \begin{itemize}
  \item Know where to put your efforts...
  \item Optimization: only useful for repeated tasks, not so much for
    interactive work
  \item Aim for improvements of orders of magnitude, not seconds
  \item But: if you write code, do it well!
  \end{itemize}
\end{frame}

\begin{frame}[fragile,containsverbatim]
  \frametitle{Sources}
  \small
  \begin{itemize}
  \item Manual ``Writing R extensions''\\
    \url{cran.r-project.org/doc/manuals/r-release/R-exts.html}
  \item CRAN Task View: High-Performance and Parallel Computing with R
  \item Vignettes for the \code{data.table} package
  \item Hadley Wickham: ``Advanced R''\\
    \url{adv-r.had.co.nz/Profiling.html#performance-profiling}
  \item Patrick Burns: ``The R Inferno''\\
    \url{www.burns-stat.com/pages/Tutor/R_inferno.pdf}
  \item Lectures by Colin Gillespie at UseR! 2017, Brussels\\
    \url{https://channel9.msdn.com/Events/useR-international-R-User-conferences/useR-International-R-User-2017-Conference/Efficient-R-Programming}
  \end{itemize}
\end{frame}

\end{document}
