graph TB
  A((Start))-->B{<strong>Find bottleneck?</strong>}
  B-->|Yes| C[<strong>Eliminate it</strong>]
  C-->B
  B-->|No| D(Optimized Code)
  # A(Start)-->B[<strong>Find bottleneck</strong>]
  # B-->C{Yes/No?}
  # C-->|Yes| D[<strong>Eliminate it</strong>]
  # C-->|No| E(Optimized Code)
  # D-->B
