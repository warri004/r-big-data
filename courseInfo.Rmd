---
title: "Course information"
author: "Sven Warris"
date: "June 1/2, 2023"
output: pdf_document
---

Welcome to the *Effective Analysis of Big Data in R*. This document contains some basic information on the course. 

## Course introduction

For this course a basic understanding of R is required: this is not a R course.
The main purposes of this course are summarized as: 

* Introduction to Big Data and the similarities and differences between regular
  modeling approaches and big data modeling
* Understanding of possibilities and limitations of R in big data research
* Introduction to high performance computing
* Reproducible research 

The aim of the course is to help experienced R users to tackle the problems they
face when analyzing big data sets. The course will consist of a mixture of
lectures and computer labs (in a ratio of approximately 60/40) so there is
plenty of time for hands-on exercises.


## Course code repository

[Main Wageningen GIT repository Effective Analysis of Big Data in R](https://git.wageningenur.nl/warri004/r-big-data)

## The lecturers

* Maikel Verouden
* Sven Warris

## Programme

Location: *B8020, Orion, building number 103, Bronland 1, 6708 WH Wageningen*. Please note that you have to bring your own laptop. Lunch is provided and there will be coffee breaks. The schedule is as follows:

### Thursday, June 1

* 09:00 Get to know each other & introduction 
* 09:30 The Big Data issue by Maikel Verouden 
* 10:30 Break 
* 11:00 Profiling and efficient R programming by Maikel Verouden
* 12:30 Lunch 
* 13:30 Profiling & efficient R programming (contd.) by Maikel Verouden 
* 15:00 Break 
* 15:30 Reproducible research by Sven Warris 
* 17:00 Q&A & Closing day 1 

### Friday, June 2

* 09:00 R memory management and big data by Maikel Verouden 
* 10:30 Break 
* 11:00 HPC in R by Sven Warris 
* 12:30 Lunch 
* 13:30 Big data analyses: a case of machine learning by Sven Warris 
* 15:00 Break 
* 15:30 Big data analyses: a case of machine learning (contd.) by Sven Warris 
* 16:30 Q&A 
* 17:00 Closing course


## Course information

Additional information can be found at:

* [Slides](https://git.wageningenur.nl/warri004/r-big-data/tree/master/slides)
* [Course
  documentation](https://git.wageningenur.nl/warri004/r-big-data/raw/master/courseInfo.pdf)
* [Data](https://git.wageningenur.nl/warri004/r-big-data/tree/master/data)
* [Examples](https://git.wageningenur.nl/warri004/r-big-data/tree/master/examples)
  
  






