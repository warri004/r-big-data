Effective Analysis of Big Data in R
==========

Welcome to Effective Analysis of Big Data in R Course repository. This contains information for a number
of editions of the course, under several names and with slightly
different content:

* [Slides](https://git.wageningenur.nl/warri004/r-big-data/tree/master/slides)
* [Course documentation, edition: 2021](https://git.wageningenur.nl/warri004/r-big-data/raw/master/courseInfo.pdf)
* [Data](https://git.wageningenur.nl/warri004/r-big-data/tree/master/data)
* [Examples](https://git.wageningenur.nl/warri004/r-big-data/tree/master/examples)


